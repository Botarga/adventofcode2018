﻿using System;
using System.IO;
using System.Collections.Generic;

namespace AdventOfCode2018
{
    abstract class DayExercise
    {
        protected string m_Input;
        private string m_DayExTitle;

        public DayExercise(string title, string input)
        {
            m_Input = input;
            m_DayExTitle = title;
        }

        public void Run()
        {
            Console.WriteLine(m_DayExTitle);
            Console.WriteLine("Part one: {0}", PartOne());
            Console.WriteLine("Part two: {0}", PartTwo());
            Console.WriteLine();
        }

        protected abstract string PartOne();
        protected abstract string PartTwo();
    }

    class Program
    {
        static string INPUT_FOLDER_PATH = "inputs" + Path.DirectorySeparatorChar;
        static List<DayExercise> exercises = new List<DayExercise>() {
            { new Day01("--- Day 1: Chronal Calibration ---",           INPUT_FOLDER_PATH + "input_1A.txt") },
            { new Day02("--- Day 2: Inventory Management System ---",   INPUT_FOLDER_PATH + "input_2A.txt") },
            { new Day03("--- Day 3: No Matter How You Slice It ---",    INPUT_FOLDER_PATH + "input_3A.txt") },
            { new Day04("--- Day 4: Repose Record ---",                 INPUT_FOLDER_PATH + "input_4A.txt") },
            { new Day05("", INPUT_FOLDER_PATH + "input_5A.txt") },
            { new Day06("", INPUT_FOLDER_PATH + "input_6A.txt") },
            { new Day07("", INPUT_FOLDER_PATH + "input_7A.txt") },
            { new Day08("", INPUT_FOLDER_PATH + "input_8A.txt") },
            { new Day09("", INPUT_FOLDER_PATH + "input_9A.txt") },
            { new Day10("", INPUT_FOLDER_PATH + "input_10A.txt") },
            { new Day11("", INPUT_FOLDER_PATH + "input_11A.txt") },
            { new Day12("", INPUT_FOLDER_PATH + "input_12A.txt") },
            { new Day13("", INPUT_FOLDER_PATH + "input_13A.txt") },
            { new Day14("", INPUT_FOLDER_PATH + "input_14A.txt") },
            { new Day15("", INPUT_FOLDER_PATH + "input_15A.txt") },
            { new Day16("", INPUT_FOLDER_PATH + "input_16A.txt") },
            { new Day17("", INPUT_FOLDER_PATH + "input_17A.txt") },
            { new Day18("", INPUT_FOLDER_PATH + "input_18A.txt") },
            { new Day19("", INPUT_FOLDER_PATH + "input_19A.txt") },
            { new Day20("", INPUT_FOLDER_PATH + "input_20A.txt") },
            { new Day21("", INPUT_FOLDER_PATH + "input_21A.txt") },
            { new Day22("", INPUT_FOLDER_PATH + "input_22A.txt") },
            { new Day23("", INPUT_FOLDER_PATH + "input_23A.txt") },
            { new Day24("", INPUT_FOLDER_PATH + "input_24A.txt") },
            { new Day25("", INPUT_FOLDER_PATH + "input_25A.txt") }
        };

        static void Main(string[] args)
        {
            try{
                exercises[3].Run();
            }
            catch (NotImplementedException){
                Console.WriteLine("This day exercise it's not implemented yet!!");
            }catch(Exception ex){
                Console.Write("Error: " + ex.Message);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace AdventOfCode2018
{
    class Day01 : DayExercise
    {
        public Day01(string title, string input) : base(title, input) { }

        protected override string PartOne()
        {
            return "" + Array.ConvertAll(File.ReadAllLines(m_Input), int.Parse).Sum();
        }

        protected override string PartTwo()
        {
            int result      = 0;
            var freqSet     = new HashSet<int>();
            string[] inputs = File.ReadAllLines(m_Input);

            bool founded = false;
            freqSet.Add(0);
            while (!founded)
            {
                foreach (string input in inputs)
                {
                    result += Convert.ToInt32(input);
                    if (freqSet.Contains(result))
                    {
                        founded = true;
                        break;
                    }
                    else
                        freqSet.Add(result);
                }
            }

            return "" + result;
        }

    }
}

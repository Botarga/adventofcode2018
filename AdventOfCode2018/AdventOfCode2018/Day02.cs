﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;

namespace AdventOfCode2018
{
    class Day02 : DayExercise
    {
        public Day02(string title, string input) : base(title, input){}

        protected override string PartOne()
        {
            int finalResult = 1;
            int[] wordLetterOcurrences;
            var finalOcurrencesDic = new ConcurrentDictionary<int, int>();

            foreach (string input in File.ReadAllLines(m_Input))
            {
                var auxParserSet = new HashSet<int>();
                wordLetterOcurrences = new int[26];
                foreach (char c in input)
                    wordLetterOcurrences[c - 'a']++;
                foreach (int oc in wordLetterOcurrences)
                    if (oc > 1)
                        auxParserSet.Add(oc);

                foreach (int setEntry in auxParserSet)
                    finalOcurrencesDic.AddOrUpdate(setEntry, 1, (id, count) => count + 1);
            }

            foreach (var kvp in finalOcurrencesDic)
                finalResult *= kvp.Value;

            return "" + finalResult;
        }

        protected override string PartTwo()
        {
            string      result                      = "";
            int         letterDifferencePosition    = 0;
            int         answerWordPosition          = 0;
            string[]    inputs                      = File.ReadAllLines(m_Input);

            bool founded = false;
            for (int i = 0; i < inputs.Length - 1 && !founded; ++i)
            {
                for(int j = i + 1; j < inputs.Length && !founded; ++j)
                {
                    int nDifferences = 0;
                    bool discard = false;
                    for(int k = 0; k < inputs[i].Length && !discard; ++k)
                    {
                        if(inputs[i][k] != inputs[j][k])
                        {
                            if (nDifferences < 1)
                            {
                                letterDifferencePosition = k;
                                nDifferences++;
                            }
                            else
                                discard = true;
                        }
                    }

                    if(!discard && nDifferences == 1)
                    {
                        answerWordPosition = i;
                        founded = true;
                    }
                }
            }

            result = inputs[answerWordPosition];
            result = result.Remove(letterDifferencePosition, 1);

            return result;
        }

    }
}

﻿using System;
using System.IO;

namespace AdventOfCode2018
{

    class Day03 : DayExercise
    {
        private class Box
        {
            public int ID;
            public int TopOffset;
            public int LeftOffset;
            public int Width;
            public int Height;

            public Box(string inp)
            {
                string[] rawData = inp.Split("@".ToCharArray(), StringSplitOptions.None);
                string[] offsetDimData = rawData[1].Split(":".ToCharArray(), StringSplitOptions.None);
                string[] offsets = offsetDimData[0].Trim().Split(",".ToCharArray(), StringSplitOptions.None);
                string[] dim = offsetDimData[1].Trim().Split("x".ToCharArray(), StringSplitOptions.None);

                ID          = Convert.ToInt32(rawData[0].Trim().Substring(1));
                LeftOffset  = Convert.ToInt32(offsets[0]);
                TopOffset   = Convert.ToInt32(offsets[1]);
                Width       = Convert.ToInt32(dim[0]);
                Height      = Convert.ToInt32(dim[1]);
            }
        }

        public Day03(string title, string input) : base(title, input){}

        protected override string PartOne()
        {
            int result = 0;
            char[,] fabric = new char[1000, 1000];

            foreach (string inp in File.ReadAllLines(m_Input)) {
                Box b = new Box(inp);

                for(int i = b.TopOffset; i < b.TopOffset + b.Height; ++i)
                {
                    for(int j = b.LeftOffset; j < b.LeftOffset + b.Width; ++j)
                    {
                        if(fabric[i,j] == 0)
                            fabric[i, j] = 'x';
                        else if (fabric[i,j] == 'x')
                        {
                            fabric[i, j] = '#';
                            result++;
                        }

                    }
                }
            }

            return "" + result;
        }

        protected override string PartTwo()
        {
            int result = 0;
            char[,] fabric = new char[1000, 1000];

            // Mark map
            foreach (string inp in File.ReadAllLines(m_Input))
            {
                Box b = new Box(inp);
                for (int i = b.TopOffset; i < b.TopOffset + b.Height; ++i)
                {
                    for (int j = b.LeftOffset; j < b.LeftOffset + b.Width; ++j)
                    {
                        if (fabric[i, j] == 0)
                            fabric[i, j] = 'x';
                        else if (fabric[i, j] == 'x')
                            fabric[i, j] = '#';
                    }
                }
            }
            // Check for collisions
            foreach (string inp in File.ReadAllLines(m_Input))
            {
                Box b = new Box(inp);

                bool collapse = false;
                for (int i = b.TopOffset; i < b.TopOffset + b.Height && !collapse; ++i)
                    for (int j = b.LeftOffset; j < b.LeftOffset + b.Width && !collapse; ++j)
                        if (fabric[i, j] == '#')
                            collapse = true;

                if (!collapse)
                    result = b.ID;
            }


            return "" + result;
        }
    }
}
